{ config, pkgs, ... }:

let
  secret = import ./secret.nix;
in
{
  imports =
    [
      ./hardware-configuration.nix
      ./packet.nix
    ];

  boot = {
    cleanTmpDir = true;
    loader.grub = {
      enable = true;
      version = 2;
    };
  };

  environment = {
    systemPackages = with pkgs; [
      ag
      caddy
      curl
      dos2unix
      elinks
      emacs
      file
      gitFull
      gitAndTools.tig
      glibc
      gnumake
      gnupg
      iotop
      libelf
      lynx
      nix-prefetch-git
      nix-repl
      nodejs
      patchelf
      sl
      su
      tmux
      tree
      unrar
      unzip
      vim
      wget
      which
      zip
    ];
  };

  networking.firewall = {
    enable = true;
    allowedTCPPorts = [ ];
    allowedUDPPorts = [ ];
  };

  nix = {
    binaryCachePublicKeys = [
      "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI="
    ];
    trustedBinaryCaches = [
      "https://nixcache.reflex-frp.org"
    ];
    distributedBuilds = false;
    extraOptions = "auto-optimise-store = true";
    nixPath = ["/etc/nixos" "nixos-config=/etc/nixos/configuration.nix"];
  };

  nixpkgs.config = {
    allowUnfree = true;
  };

  programs = {
    bash.enableCompletion = true;
    mosh.enable = true;
  };

  time.timeZone = "Pacific/Auckland";

  users.extraUsers.jdh = {
    extraGroups = ["networkmanager" "wheel"];
    isNormalUser = true;
    uid = 1001;
  };

  security = {
    pam = {
      enableSSHAgentAuth = true;
      services.sudo.sshAgentAuth = true;
    };
    sudo = {
      enable = true;
      wheelNeedsPassword = true;
   };
  };

  services = {
    gitlab-runner = {
      enable = true;
      enableShellExecutor = true;
      configFile = "/etc/gitlab-runner.cfg";
    };
    nix-serve = {
      enable = true;
      bindAddress = "0.0.0.0";
      port = 5000;
      secretKeyFile = "/etc/secret/nix-binary-cache-key";
    };
    openssh = {
      enable = true;
      challengeResponseAuthentication = false;
      passwordAuthentication = false;
      startWhenNeeded = false;
      extraConfig = ''
        StreamLocalBindUnlink yes
      '';
    };
  };

  system = {
    stateVersion = "17.03";
  };
}
