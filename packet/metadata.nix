{ 
      networking.hostName = "colossus";
    
      networking.bonds.bond0 = {
        driverOptions.mode = "balance-tlb";
        interfaces = [
          "enp0s20f0" "enp0s20f1"
        ];
      };
    
      networking.interfaces.bond0 = {
        useDHCP = true;

        ip4 = [
          
          {
            address = "147.75.92.71";
            prefixLength = 31;
          }
    

          {
            address = "10.64.7.1";
            prefixLength = 31;
          }
    
        ];

        ip6 = [
          
          {
            address = "2604:1380:3000:4600::1";
            prefixLength = 127;
          }
    
        ];
      };
    
      users.users.root.openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCvu310rcSbHGQfaBVIgIExBs5knHNpUU2mPg+lpggpc+7AXhwYFYCLgb0QTuGWV5u2Lc+uQqDIMIYd3ygdMLa/TwDfwemjid/qJGsJ0AvKiPTFxI6iO9/+ZaYd8aH5jSYTttTid8JY2Pq+cCB068fM2UOMZoc5pIlUJcdvJ6UiiI5G1h04zlneZbNu0JZ9n0tqfY77sH8RlMIPT918VjttVmThl5T66cZ+sEomCt0Fr5qB46X3TNkCUNdwmN+h0zvNQLkzWIfBa+umQROb09T1krLWvMBuppQlIuGqyiZvEKBoSVwWgcGhV/EBVFxFSZP79Ne8vqIZCwJji4tXqSI1"
    
      ];
     }
